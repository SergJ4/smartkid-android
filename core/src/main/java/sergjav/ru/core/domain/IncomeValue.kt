package sergjav.ru.core.domain

import android.text.Editable
import android.text.TextWatcher
import sergjav.ru.core.CachedField
import javax.inject.Inject


class IncomeValue @Inject constructor() : CachedField<String>(), TextWatcher {

    override fun afterTextChanged(newText: Editable) {
        this(newText.toString())
    }

    override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(newText: CharSequence, start: Int, before: Int, count: Int) {
    }
}

