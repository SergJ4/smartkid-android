package sergjav.ru.core.domain

import android.support.v4.widget.SwipeRefreshLayout
import sergjav.ru.core.ObservableField
import sergjav.ru.domain.model.Trigger
import javax.inject.Inject


class SwipeRefresh @Inject constructor() : ObservableField<Trigger<Any?>>(),
    SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
        this(Trigger())
    }
}