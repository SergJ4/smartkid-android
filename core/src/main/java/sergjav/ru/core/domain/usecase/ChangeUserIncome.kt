package sergjav.ru.core.domain.usecase

import io.reactivex.Single
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.domain.model.DomainUser
import javax.inject.Inject


class ChangeUserIncome @Inject constructor(
    private val userRepository: UserRepository
) {

    operator fun invoke(newIncome: Double): Single<DomainUser> =
        userRepository
            .changeUserIncome(newIncome)
}