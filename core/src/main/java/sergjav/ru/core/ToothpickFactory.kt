package sergjav.ru.core

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import toothpick.Scope

class ToothpickFactory(private val scope: Scope) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = scope.getInstance(modelClass)
}