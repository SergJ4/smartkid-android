package sergjav.ru.core.base

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import sergjav.ru.core.ToothpickFactory
import sergjav.ru.core.di.BaseActivityModule
import toothpick.Scope
import toothpick.Toothpick
import toothpick.config.Module
import toothpick.smoothie.module.SmoothieActivityModule

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    private var activityId: Int = 0

    protected abstract val contentView: Int
    protected abstract val module: Array<Module>
    protected abstract val viewModelClass: Class<VM>
    protected lateinit var viewModel: VM

    internal lateinit var activityScope: Scope

    val activityName
        get() = "${javaClass.canonicalName}-$activityId"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(contentView)

        activityId = hashCode()
        activityScope = Toothpick.openScopes(application, activityName)
        activityScope.installModules(
            SmoothieActivityModule(this),
            BaseActivityModule(this),
            *module
        )

        Toothpick.inject(this, activityScope)

        viewModel = ViewModelProviders
            .of(this, ToothpickFactory(activityScope))
            .get(viewModelClass)

        setup(savedInstanceState)
    }

    protected abstract fun setup(savedInstanceState: Bundle?)

    override fun onDestroy() {
        Toothpick.closeScope(activityName)
        super.onDestroy()
    }
}