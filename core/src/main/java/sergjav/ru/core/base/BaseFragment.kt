package sergjav.ru.core.base

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import org.jetbrains.anko.findOptional
import sergjav.ru.core.R
import sergjav.ru.core.ToothpickFactory
import sergjav.ru.core.di.BaseFragmentModule
import sergjav.ru.core.extensions.applyDefaultStyle
import sergjav.ru.domain.interfaces.MessageBus
import sergjav.ru.domain.interfaces.ProgressBus
import toothpick.Scope
import toothpick.Toothpick
import toothpick.config.Module
import javax.inject.Inject

abstract class BaseFragment<VM : BaseViewModel> : Fragment() {

    private lateinit var fragmentScope: Scope
    private var fragmentId: Int = 0
    private val fragmentName
        get() = "${javaClass.canonicalName}-$fragmentId"

    protected abstract val contentView: Int
    protected open var modules: Array<Module> = arrayOf()
    protected abstract val viewModelClass: Class<VM>
    protected lateinit var viewModel: VM

    @Inject
    protected lateinit var messageBus: MessageBus
    @Inject
    protected lateinit var progressBus: ProgressBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentId = hashCode()
        fragmentScope =
                Toothpick.openScopes((activity as BaseActivity<*>).activityName, fragmentName)
        fragmentScope.installModules(BaseFragmentModule(this), *modules)

        Toothpick.inject(this, fragmentScope)

        viewModel = ViewModelProviders
            .of(this, ToothpickFactory(fragmentScope))
            .get(viewModelClass)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(contentView, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        messageBus.anchorView = view

        val swipeRefresh = view.findOptional<SwipeRefreshLayout>(R.id.swipeRefresh)
        swipeRefresh?.applyDefaultStyle()

        progressBus.attachView(swipeRefresh)
        view.findOptional<ImageView>(R.id.backArrow)?.setOnClickListener {
            viewModel.back()
        }

        setup()
    }

    protected abstract fun setup()

    override fun onDestroyView() {
        messageBus.anchorView = null
        progressBus.attachView(null)
        super.onDestroyView()
    }

    override fun onDestroy() {
        Toothpick.closeScope(fragmentName)
        super.onDestroy()
    }
}