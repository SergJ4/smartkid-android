package sergjav.ru.core.base

import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import sergjav.ru.core.SchedulersProvider
import sergjav.ru.domain.exceptions.defaultErrorHandler
import sergjav.ru.domain.interfaces.Logger
import sergjav.ru.domain.interfaces.MessageBus
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.Router

abstract class BaseViewModel(
    protected val router: Router,
    protected val messageBus: MessageBus,
    protected val progressBus: ProgressBus,
    protected val logger: Logger
) : ViewModel() {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun back() = router.back()

    protected fun <T> Single<T>.subscribeAsync(
        scheduler: Scheduler = SchedulersProvider.io(),
        onError: (Throwable) -> Unit = { throwable ->
            defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
            progressBus.showProgress(false)
            logger.e(throwable)
        },
        onSuccess: (T) -> Unit = {}
    ) {
        compositeDisposable
            .add(
                this
                    .subscribeOn(scheduler)
                    .observeOn(SchedulersProvider.ui())
                    .subscribe(onSuccess, onError)
            )
    }

    protected fun <T> Observable<T>.subscribeAsync(
        scheduler: Scheduler = SchedulersProvider.io(),
        onError: (Throwable) -> Unit = { throwable ->
            defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
            progressBus.showProgress(false)
            logger.e(throwable)
        },
        onComplete: () -> Unit = {},
        onNext: (T) -> Unit = {}
    ) {
        compositeDisposable
            .add(
                this
                    .subscribeOn(scheduler)
                    .observeOn(SchedulersProvider.ui())
                    .subscribe(onNext, onError, onComplete)
            )
    }
}