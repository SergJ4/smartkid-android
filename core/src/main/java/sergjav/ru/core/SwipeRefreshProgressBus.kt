package sergjav.ru.core

import android.os.Handler
import android.os.Looper
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import sergjav.ru.domain.interfaces.ProgressBus


class SwipeRefreshProgressBus : ProgressBus {

    var swipeRefresh: SwipeRefreshLayout? = null

    override fun showProgress(shouldShow: Boolean) {
        Handler(Looper.getMainLooper()).post {
            swipeRefresh?.isRefreshing = shouldShow
        }
    }

    override fun attachView(view: View?) {
        if ((view != null && view is SwipeRefreshLayout) || view == null) {
            swipeRefresh = view as? SwipeRefreshLayout
        } else {
            throw IllegalArgumentException("View must be a SwipeRefreshLayout")
        }
    }
}