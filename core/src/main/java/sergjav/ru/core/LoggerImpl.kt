package sergjav.ru.core

import sergjav.ru.domain.interfaces.Logger
import timber.log.Timber
import javax.inject.Inject

class LoggerImpl @Inject constructor() : Logger {

    override fun d(message: String) = Timber.d(message)

    override fun d(message: String, tag: String) = Timber.tag(tag).d(message)

    override fun e(message: String) = Timber.e(message)

    override fun e(throwable: Throwable) = Timber.e(throwable)

    override fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}