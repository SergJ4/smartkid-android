package sergjav.ru.core

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

abstract class ObservableField<T> {

    private val subject: PublishSubject<T> = PublishSubject.create()

    operator fun invoke(newValue: T) = subject.onNext(newValue)

    operator fun invoke(): Observable<T> = subject.hide()
}