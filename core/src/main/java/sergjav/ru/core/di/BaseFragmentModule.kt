package sergjav.ru.core.di

import android.support.v4.app.Fragment
import toothpick.config.Module

class BaseFragmentModule(fragment: Fragment) : Module() {

    init {
        bind(Fragment::class.java).toInstance(fragment)
    }
}