package sergjav.ru.core.di

import android.content.Context
import sergjav.ru.core.base.BaseActivity
import toothpick.config.Module

class BaseActivityModule(
    activity: BaseActivity<*>
) : Module() {

    init {
        bind(Context::class.java).toInstance(activity)
    }


}