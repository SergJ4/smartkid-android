package sergjav.ru.core.extensions

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import android.view.inputmethod.InputMethodManager
import org.jetbrains.anko.dip
import sergjav.ru.core.R

fun SwipeRefreshLayout.applyDefaultStyle(progressViewOffset: Int = 60) {
    setSize(SwipeRefreshLayout.DEFAULT)
    setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_light)
    setDistanceToTriggerSync(200)
    setProgressViewOffset(true, 0, dip(progressViewOffset))
}

fun Context.color(@ColorRes colorRes: Int): Int = ContextCompat.getColor(this, colorRes)

fun Fragment.color(@ColorRes colorRes: Int): Int = ContextCompat.getColor(context!!, colorRes)

fun View.color(@ColorRes colorRes: Int): Int = ContextCompat.getColor(context!!, colorRes)

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(windowToken, 0)
}