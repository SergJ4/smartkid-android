package sergjav.ru.core.extensions

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData

operator fun <T> MutableLiveData<T>.invoke(newValue: T) {
    value = newValue
}

fun <T> LiveData<T>.subscribe(lifecycle: Lifecycle, action: (T?) -> Unit) {
    observe({ lifecycle }) {
        action(it)
    }
}

fun <T> LiveData<T>.filter(predicate: (T) -> Boolean): LiveData<T> {
    val result = MediatorLiveData<T>()

    result.addSource(this) {
        if (predicate(it!!)) result.value = it
    }

    return result
}

fun <T> MutableLiveData<T>.startWith(defaultValue: T): MutableLiveData<T> =
    this.apply { value = defaultValue }