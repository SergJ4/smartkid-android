package sergjav.ru.core.extensions

import android.text.TextUtils

fun String.isNumeric() = TextUtils.isDigitsOnly(this)