package sergjav.ru.core

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.util.AttributeSet

/**
 * Created by zhavoronkov on 17.01.2018.
 */
class SwipeRefreshWithoutSwipe : SwipeRefreshLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    override fun canChildScrollUp(): Boolean = true
}