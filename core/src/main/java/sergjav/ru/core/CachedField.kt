package sergjav.ru.core

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


abstract class CachedField<T>() {

    private val subject: BehaviorSubject<T> = BehaviorSubject.create()

    constructor(startWith: T) : this() {
        subject.onNext(startWith)
    }

    operator fun invoke(newValue: T) = subject.onNext(newValue)

    operator fun invoke(): Observable<T> = subject.hide()
}