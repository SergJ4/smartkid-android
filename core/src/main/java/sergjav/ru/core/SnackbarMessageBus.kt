package sergjav.ru.core

import android.support.design.widget.Snackbar
import android.view.View
import android.widget.TextView
import org.jetbrains.anko.find
import sergjav.ru.core.extensions.color
import sergjav.ru.domain.interfaces.MessageBus


class SnackbarMessageBus : MessageBus {

    override var anchorView: View? = null

    override fun showMessage(message: String, resId: Int) {
        anchorView?.let {
            val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            if (resId > 0) {
                snackbar.setText(resId)
            }
            val text = snackbar.view.find<TextView>(android.support.design.R.id.snackbar_text)
            text.setTextColor(it.context!!.color(R.color.snackbar_text_color))
            text.textSize = 14f
            snackbar.show()
        }
    }

    override fun showMessage(exception: Throwable) {
        if (exception.message.isNullOrBlank()) {
            showMessage(resId = R.string.unknown_error)
        } else {
            showMessage(exception.message!!)
        }
    }
}