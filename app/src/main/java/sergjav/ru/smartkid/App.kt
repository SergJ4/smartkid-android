package sergjav.ru.smartkid

import android.app.Application
import sergjav.ru.core.PREFERENCES
import sergjav.ru.domain.interfaces.Logger
import toothpick.Toothpick
import toothpick.smoothie.module.SmoothieApplicationModule
import javax.inject.Inject


class App : Application() {

    @Inject
    internal lateinit var logger: Logger

    override fun onCreate() {
        super.onCreate()
        val appScope = Toothpick.openScope(this)
        appScope.installModules(
            SmoothieApplicationModule(this, PREFERENCES),
            ApplicationModule(this)
        )
        Toothpick.inject(this, appScope)
        logger.init()
    }
}