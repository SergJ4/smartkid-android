package sergjav.ru.smartkid

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import sergjav.ru.auth.AuthFragment
import sergjav.ru.auth.login.LoginFragment
import sergjav.ru.auth.registration.first.FirstRegistrationFragment
import sergjav.ru.auth.registration.income.IncomeFragment
import sergjav.ru.domain.interfaces.*
import sergjav.ru.profile.ProfileFragment
import sergjav.ru.profile.wishlist.WishlistFragment


class AppNavigator(
    fragmentManager: FragmentManager,
    containerId: Int,
    private val activity: MainActivity
) :
    SupportFragmentNavigator(fragmentManager, containerId) {

    override fun exit() = activity.finish()

    override fun createFragment(screenKey: String, data: Any?): Fragment =
        screenKey.toFragment(data)

    override fun showSystemMessage(message: String?) {
    }
}

private fun String.toFragment(data: Any?): Fragment = when (this) {
    AUTH_SCREEN -> AuthFragment.getInstance()
    LOGIN_SCREEN -> LoginFragment.getInstance()
    FIRST_REGISTRATION_SCREEN -> FirstRegistrationFragment.getInstance()
    INCOME_REGISTRATION_SCREEN -> IncomeFragment.getInstance()
    PROFILE_SCREEN -> ProfileFragment.getInstance()
    WISH_LIST_SCREEN -> WishlistFragment.getInstance()
    else -> throw IllegalArgumentException("Unknown screen name: $this")
}