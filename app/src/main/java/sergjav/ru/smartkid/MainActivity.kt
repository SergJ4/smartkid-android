package sergjav.ru.smartkid

import android.os.Bundle
import ru.terrakok.cicerone.NavigatorHolder
import sergjav.ru.auth.domain.RegistrationDataContainer
import sergjav.ru.core.base.BaseActivity
import toothpick.config.Module
import javax.inject.Inject

class MainActivity : BaseActivity<MainViewModel>() {

    override val contentView: Int = R.layout.main_activity_layout

    override val module: Array<Module> = arrayOf(object : Module() {
        init {
            bind(RegistrationDataContainer::class.java).singletonInScope()
        }
    })

    override val viewModelClass: Class<MainViewModel> = MainViewModel::class.java

    @Inject
    internal lateinit var navigatorHolder: NavigatorHolder

    private val navigator: AppNavigator =
        AppNavigator(supportFragmentManager, R.id.fragmentsContainer, this)

    override fun setup(savedInstanceState: Bundle?) {
        viewModel.handleNavigation(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}