package sergjav.ru.smartkid

import android.os.Bundle
import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.domain.interfaces.*
import sergjav.ru.domain.interfaces.repository.LocalRepo
import sergjav.ru.repository.getUserId
import javax.inject.Inject


class MainViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger,
    private val localRepo: LocalRepo
) : BaseViewModel(
    router = router,
    messageBus = messageBus,
    progressBus = progressBus,
    logger = logger
) {
    fun handleNavigation(savedInstanceState: Bundle?) {
        if (savedInstanceState == null && localRepo.getUserId().isEmpty()) {
            router.rootScreen(AUTH_SCREEN)
        } else if (savedInstanceState == null) {
            router.rootScreen(PROFILE_SCREEN)
        }
    }
}