package sergjav.ru.smartkid

import android.content.Context
import retrofit2.Retrofit
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import sergjav.ru.core.LoggerImpl
import sergjav.ru.core.SnackbarMessageBus
import sergjav.ru.core.SwipeRefreshProgressBus
import sergjav.ru.domain.interfaces.Logger
import sergjav.ru.domain.interfaces.MessageBus
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.repository.LocalRepo
import sergjav.ru.domain.interfaces.repository.UserDataSource
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.repository.NetworkUserDataSource
import sergjav.ru.repository.SharedPrefsRepo
import sergjav.ru.repository.UserRepositoryImpl
import sergjav.ru.repository.initRepository
import toothpick.config.Module


class ApplicationModule(app: App) : Module() {

    init {
        val cicerone = Cicerone.create()
        val router = cicerone.router
        val navHolder = cicerone.navigatorHolder
        bind(Context::class.java).toInstance(app.applicationContext)
        bind(MessageBus::class.java).toInstance(SnackbarMessageBus())
        bind(ProgressBus::class.java).toInstance(SwipeRefreshProgressBus())
        bind(LocalRepo::class.java).to(SharedPrefsRepo::class.java).singletonInScope()
        bind(UserRepository::class.java).to(UserRepositoryImpl::class.java).singletonInScope()
        bind(Logger::class.java).to(LoggerImpl::class.java).singletonInScope()
        bind(Retrofit::class.java).toInstance(initRepository(app.applicationContext))
        bind(UserDataSource::class.java).to(NetworkUserDataSource::class.java).singletonInScope()
        bind(Router::class.java).toInstance(router)
        bind(NavigatorHolder::class.java).toInstance(navHolder)
        bind(sergjav.ru.domain.interfaces.Router::class.java).to(ApplicationRouter::class.java)
    }
}