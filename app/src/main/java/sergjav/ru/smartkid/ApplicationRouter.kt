package sergjav.ru.smartkid

import sergjav.ru.domain.interfaces.Router
import javax.inject.Inject

class ApplicationRouter @Inject constructor(
    private val router: ru.terrakok.cicerone.Router
) : Router {

    override fun goTo(screenName: String, data: Any?) {
        router.navigateTo(screenName, data)
    }

    override fun rootScreen(screenName: String) {
        router.newRootScreen(screenName)
    }

    override fun back() {
        router.exit()
    }
}
