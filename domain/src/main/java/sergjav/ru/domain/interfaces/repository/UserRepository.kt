package sergjav.ru.domain.interfaces.repository

import io.reactivex.Observable
import io.reactivex.Single
import sergjav.ru.domain.model.DomainUser

interface UserRepository {

    val userDataSource: UserDataSource

    fun user(id: String): Observable<DomainUser>

    fun getLocalUser(): Single<DomainUser>

    fun registerNewUser(
        email: String,
        password: String,
        name: String,
        birthdayTimestamp: Long,
        dayNumToIncrementMoney: Int
    ): Single<DomainUser>

    fun login(
        email: String,
        password: String
    ): Single<DomainUser>

    fun isLoggedIn(): Boolean

    fun validateEmail(email: String): Single<Boolean>

    fun changeUserIncome(newIncome: Double): Single<DomainUser>
}