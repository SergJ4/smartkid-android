package sergjav.ru.domain.interfaces.repository

import arrow.core.Option


interface LocalRepo {

    fun save(key: String, value: Any)

    fun get(key: String): Option<Any>

    fun exists(key: String): Boolean
}