package sergjav.ru.domain.interfaces

const val AUTH_SCREEN = "authentication_screen"
const val LOGIN_SCREEN = "login_screen"
const val FIRST_REGISTRATION_SCREEN = "first_registration_screen"
const val INCOME_REGISTRATION_SCREEN = "third_registration_screen"
const val PROFILE_SCREEN = "profile_screen"
const val WISH_LIST_SCREEN = "wish_list_screen"

interface Router {

    fun goTo(screenName: String, data: Any? = null)

    fun rootScreen(screenName: String)

    fun back()
}