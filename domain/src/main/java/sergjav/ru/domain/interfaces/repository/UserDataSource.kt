package sergjav.ru.domain.interfaces.repository

import io.reactivex.Single
import sergjav.ru.domain.model.DomainUser

interface UserDataSource {

    fun getUserById(id: String): Single<DomainUser>

    fun registerNewUser(
        email: String,
        password: String,
        name: String,
        birthdayTimestamp: Long,
        dayNumToIncrementMoney: Int
    ): Single<DomainUser>

    fun login(
        email: String,
        password: String
    ): Single<DomainUser>

    fun changeUserIncome(
        userId: String,
        newIncome: Double
    ): Single<DomainUser>

    fun validateEmail(email: String): Single<Boolean>
}