package sergjav.ru.domain.exceptions

import java.io.IOException

/**
 * Данное исключение выкидывается в случае получения ошибок, которые будут в presenter
 */
data class SimpleMessageException(val errorId: Int = 0, val exceptionMessage: String = "") :
    IOException()
