package sergjav.ru.domain.exceptions

import java.io.IOException


fun defaultErrorHandler(
    throwable: Throwable,
    errorMessage: (String) -> Unit
) {

    when (throwable) {
        is RuntimeException -> {
            val cause = throwable.cause
            when (cause) {
                is SimpleMessageException -> errorMessage(cause.exceptionMessage)
                is IOException -> errorMessage(cause.message ?: "Unknown error")
                else -> errorMessage(throwable.message ?: "Unknown error")
            }
        }
        is SimpleMessageException -> errorMessage(throwable.exceptionMessage)
        else -> errorMessage(throwable.message ?: "Unknown error")
    }
}