package sergjav.ru.domain.model

data class DomainUser(
    val id: String,
    val name: String,
    val birthdayTimestamp: Long,
    val moneyPerWeekIncome: Double,
    val currentMoneyAmount: Double,
    val moneyIncrementWeekDay: Int,
    val wishes: List<DomainWish>
)

val dummyUser = DomainUser(
    "",
    "",
    0,
    0.0,
    0.0,
    0,
    listOf()
)