package sergjav.ru.domain.model

data class Error(
    val message: String = "",
    val resId: Int = -1,
    val exception: Throwable? = null
)