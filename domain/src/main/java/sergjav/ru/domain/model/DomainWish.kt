package sergjav.ru.domain.model

data class DomainWish(
    val id: String,
    val title: String,
    val price: Double
)