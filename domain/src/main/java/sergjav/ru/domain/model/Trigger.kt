package sergjav.ru.domain.model

data class Trigger<T>(val data: T? = null)