package sergjav.ru.profile

import android.arch.lifecycle.MutableLiveData
import io.reactivex.schedulers.Schedulers
import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.core.domain.SwipeRefresh
import sergjav.ru.core.extensions.invoke
import sergjav.ru.domain.interfaces.*
import sergjav.ru.domain.model.DomainUser
import sergjav.ru.domain.model.Trigger
import sergjav.ru.profile.domain.WishlistButtonClick
import sergjav.ru.profile.domain.usecase.GetLocalUser
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger,
    wishlistButtonClick: WishlistButtonClick,
    swipeRefresh: SwipeRefresh,
    getLocalUser: GetLocalUser
) : BaseViewModel(router, messageBus, progressBus, logger) {

    internal val userData = MutableLiveData<DomainUser>()

    init {
        progressBus.showProgress(true)
        swipeRefresh()
            .startWith(Trigger())
            .observeOn(Schedulers.io())
            .flatMapSingle { getLocalUser() }
            .subscribeAsync {
                progressBus.showProgress(false)
                userData(it)
            }

        wishlistButtonClick()
            .subscribeAsync {
                router.goTo(WISH_LIST_SCREEN)
            }
    }
}