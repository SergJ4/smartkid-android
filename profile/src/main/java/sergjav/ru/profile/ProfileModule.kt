package sergjav.ru.profile

import sergjav.ru.core.domain.SwipeRefresh
import sergjav.ru.profile.domain.WishlistButtonClick
import sergjav.ru.profile.domain.usecase.GetLocalUser
import toothpick.config.Module


class ProfileModule : Module() {

    init {
        bind(WishlistButtonClick::class.java).singletonInScope()
        bind(SwipeRefresh::class.java).singletonInScope()
        bind(GetLocalUser::class.java).singletonInScope()
    }
}