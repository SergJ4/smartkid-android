package sergjav.ru.profile.wishlist

import sergjav.ru.core.base.BaseFragment
import sergjav.ru.profile.R


class WishlistFragment : BaseFragment<WishlistViewModel>() {

    override val contentView: Int = R.layout.wishlist_layout

    override val viewModelClass: Class<WishlistViewModel> = WishlistViewModel::class.java

    override fun setup() {
        //todo
    }

    companion object {
        fun getInstance() = WishlistFragment()
    }
}