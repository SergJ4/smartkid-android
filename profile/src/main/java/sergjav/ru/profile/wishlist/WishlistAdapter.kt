package sergjav.ru.profile.wishlist

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import sergjav.ru.domain.model.DomainWish
import sergjav.ru.profile.R


class WishlistAdapter(private var wishes: List<DomainWish>) :
    RecyclerView.Adapter<WishViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WishViewHolder =
        WishViewHolder(View.inflate(parent.context, R.layout.wish_item_layout, null))


    override fun getItemCount(): Int = wishes.size

    override fun onBindViewHolder(holder: WishViewHolder, position: Int) {
        holder.bind(wishes[position])
    }
}