package sergjav.ru.profile.wishlist

import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.domain.interfaces.Logger
import sergjav.ru.domain.interfaces.MessageBus
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.Router


class WishlistViewModel(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger
) : BaseViewModel(router, messageBus, progressBus, logger) {
}