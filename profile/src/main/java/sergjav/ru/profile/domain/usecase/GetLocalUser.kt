package sergjav.ru.profile.domain.usecase

import io.reactivex.Single
import sergjav.ru.domain.exceptions.defaultErrorHandler
import sergjav.ru.domain.interfaces.Logger
import sergjav.ru.domain.interfaces.MessageBus
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.domain.model.dummyUser
import javax.inject.Inject


class GetLocalUser @Inject constructor(
    private val userRepository: UserRepository,
    private val messageBus: MessageBus,
    private val progressBus: ProgressBus,
    private val logger: Logger
) {

    operator fun invoke() = userRepository
        .getLocalUser()
        .onErrorResumeNext { throwable ->
            defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
            progressBus.showProgress(false)
            logger.e(throwable)
            Single.just(dummyUser)
        }
}