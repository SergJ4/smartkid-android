package sergjav.ru.profile.domain

import android.view.View
import sergjav.ru.core.ObservableField
import sergjav.ru.domain.AnyValue
import sergjav.ru.domain.model.Trigger
import javax.inject.Inject


class WishlistButtonClick @Inject constructor() : ObservableField<Trigger<AnyValue>>(),
    View.OnClickListener {

    override fun onClick(p0: View?) {
        this(Trigger(AnyValue))
    }
}