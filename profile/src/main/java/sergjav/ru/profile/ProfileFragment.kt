package sergjav.ru.profile

import kotlinx.android.synthetic.main.profile_layout.*
import sergjav.ru.core.base.BaseFragment
import sergjav.ru.core.domain.SwipeRefresh
import sergjav.ru.core.extensions.subscribe
import sergjav.ru.domain.model.DomainUser
import sergjav.ru.profile.domain.WishlistButtonClick
import toothpick.config.Module
import javax.inject.Inject

class ProfileFragment : BaseFragment<ProfileViewModel>() {
    override val contentView: Int = R.layout.profile_layout

    override val viewModelClass: Class<ProfileViewModel> = ProfileViewModel::class.java

    override var modules: Array<Module> = arrayOf(ProfileModule())

    @Inject
    internal lateinit var wishlistButtonClick: WishlistButtonClick
    @Inject
    internal lateinit var swipeRefreshListener: SwipeRefresh

    override fun setup() {
        addMoreWishes.setOnClickListener(wishlistButtonClick)
        swipeRefresh.setOnRefreshListener(swipeRefreshListener)

        viewModel
            .userData
            .subscribe(lifecycle) { user ->
                user?.let {
                    incomeValue.text = user.moneyPerWeekIncome.toString()
                    amountValue.text = user.currentMoneyAmount.toString()
                    allWishesCountValue.text = user.wishes.size.toString()
                    acceptableWishesCountValue.text = calculateAcceptableWishes(user).toString()
                }
            }
    }

    private fun calculateAcceptableWishes(user: DomainUser): Int =
        user
            .wishes
            .count { it.price <= user.currentMoneyAmount }

    companion object {
        fun getInstance() = ProfileFragment()
    }
}