package sergjav.ru.auth.domain.usecase

import io.reactivex.Single
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.RegistrationDataContainer
import sergjav.ru.auth.domain.WeekDayValue
import sergjav.ru.core.SchedulersProvider
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.domain.model.DomainUser
import javax.inject.Inject

class RegisterNewUser @Inject constructor(
    private val userRepository: UserRepository,
    private val registrationDataContainer: RegistrationDataContainer,
    private val weekDayValue: WeekDayValue,
    private val acceptClicked: AcceptClicked
) {

    operator fun invoke(): Single<DomainUser> =
        acceptClicked()
            .observeOn(SchedulersProvider.io())
            .flatMap { weekDayValue() }
            .take(1)
            .flatMap { incomeWeekDay ->
                userRepository
                    .registerNewUser(
                        registrationDataContainer.email,
                        registrationDataContainer.password,
                        registrationDataContainer.name,
                        registrationDataContainer.birthdayTimeStamp,
                        incomeWeekDay
                    )
                    .toObservable()
            }
            .take(1)
            .singleOrError()
}