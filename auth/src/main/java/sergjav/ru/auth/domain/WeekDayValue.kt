package sergjav.ru.auth.domain

import android.widget.NumberPicker
import sergjav.ru.core.CachedField
import javax.inject.Inject


class WeekDayValue @Inject constructor() : CachedField<Int>(), NumberPicker.OnValueChangeListener {

    override fun onValueChange(picker: NumberPicker, oldValue: Int, newValue: Int) =
        this(newValue)
}