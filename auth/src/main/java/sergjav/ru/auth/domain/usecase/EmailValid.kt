package sergjav.ru.auth.domain.usecase

import android.util.Patterns
import io.reactivex.Observable
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.EmailText
import javax.inject.Inject

class EmailValid
@Inject constructor(
    private val emailText: EmailText,
    private val acceptClicked: AcceptClicked
) {

    operator fun invoke(): Observable<Boolean> =
        acceptClicked()
            .flatMap { emailText().take(1) }
            .map { email ->
                !email.isBlank() &&
                        Patterns
                            .EMAIL_ADDRESS
                            .matcher(email)
                            .matches()
            }
}