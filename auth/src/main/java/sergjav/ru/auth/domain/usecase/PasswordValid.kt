package sergjav.ru.auth.domain.usecase

import io.reactivex.Observable
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.PasswordText
import javax.inject.Inject


class PasswordValid
@Inject constructor(
    private val passwordText: PasswordText,
    private val acceptClicked: AcceptClicked
) {

    operator fun invoke(): Observable<Boolean> = acceptClicked()
        .flatMap { passwordText().take(1) }
        .map { it.length >= 6 }
}