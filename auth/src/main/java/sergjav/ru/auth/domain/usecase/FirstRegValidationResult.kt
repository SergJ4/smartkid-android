package sergjav.ru.auth.domain.usecase

import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.core.SchedulersProvider
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.repository.UserRepository
import javax.inject.Inject

class FirstRegValidationResult @Inject constructor(
    private val userRepository: UserRepository,
    private val emailValid: EmailValid,
    private val passwordValid: PasswordValid,
    private val nameValid: NameValid,
    private val acceptClicked: AcceptClicked,
    private val emailText: EmailText,
    private val progressBus: ProgressBus
) {

    operator fun invoke(): Observable<Boolean> =
        Observable
            .combineLatest(
                emailValid(),
                passwordValid(),
                nameValid(),
                Function3 { isEmailValid: Boolean, isPasswordValid: Boolean, isNameValid: Boolean ->
                    isEmailValid && isPasswordValid && isNameValid
                }
            )
            .filter { it }
            .doOnNext { progressBus.showProgress(true) }
            .observeOn(Schedulers.io())
            .flatMap { emailText().take(1) }
            .flatMap { userRepository.validateEmail(it).toObservable() }
            .observeOn(SchedulersProvider.ui())
}