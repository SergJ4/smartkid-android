package sergjav.ru.auth.domain.usecase

import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.core.SchedulersProvider
import sergjav.ru.domain.interfaces.ProgressBus
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.domain.model.DomainUser
import javax.inject.Inject


class LoginUser @Inject constructor(
    private val userRepository: UserRepository,
    private val emailValid: EmailValid,
    private val passwordValid: PasswordValid,
    private val emailText: EmailText,
    private val passwordText: PasswordText,
    private val progressBus: ProgressBus
) {

    operator fun invoke(): Observable<DomainUser> =
        Observable
            .combineLatest(
                emailValid(),
                passwordValid(),
                BiFunction<Boolean, Boolean, Boolean> { emailValid, passwordValid ->
                    emailValid && passwordValid
                }
            )
            .filter { it }
            .doOnNext { progressBus.showProgress(true) }
            .observeOn(SchedulersProvider.io())
            .flatMap {
                Observable.combineLatest(
                    emailText(),
                    passwordText(),
                    BiFunction { email: String, password: String -> email to password }
                )
                    .take(1)
            }
            .flatMap { (email, password) ->
                userRepository
                    .login(email, password)
                    .toObservable()
            }
}