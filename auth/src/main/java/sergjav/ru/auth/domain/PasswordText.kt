package sergjav.ru.auth.domain

import android.text.Editable
import android.text.TextWatcher
import sergjav.ru.core.CachedField
import javax.inject.Inject


class PasswordText
@Inject constructor() : CachedField<String>(startWith = ""), TextWatcher {
    override fun afterTextChanged(newText: Editable) = this(newText.toString())

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}