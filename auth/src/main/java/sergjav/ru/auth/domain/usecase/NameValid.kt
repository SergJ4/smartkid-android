package sergjav.ru.auth.domain.usecase

import io.reactivex.Observable
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.NameText
import javax.inject.Inject


class NameValid
@Inject constructor(
    private val nameText: NameText,
    private val acceptClicked: AcceptClicked
) {

    operator fun invoke(): Observable<Boolean> = acceptClicked()
        .flatMap { nameText() }
        .map { it.length > 3 }
}