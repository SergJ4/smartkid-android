package sergjav.ru.auth.domain

import sergjav.ru.core.ObservableField
import sergjav.ru.domain.AnyValue
import sergjav.ru.domain.model.Trigger
import javax.inject.Inject


class AcceptClicked
@Inject constructor() : ObservableField<Trigger<AnyValue>>()