package sergjav.ru.auth.domain

import android.widget.DatePicker
import sergjav.ru.core.CachedField
import java.util.*
import javax.inject.Inject

class CalendarDate @Inject constructor() : CachedField<Long>(), DatePicker.OnDateChangedListener {

    override fun onDateChanged(p0: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        this(calendar.timeInMillis * 1000)
    }
}