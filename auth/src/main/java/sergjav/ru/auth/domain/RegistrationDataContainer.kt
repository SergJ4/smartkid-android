package sergjav.ru.auth.domain

import javax.inject.Inject


class RegistrationDataContainer @Inject constructor() {
    var email: String = ""
    var password: String = ""
    var name: String = ""
    var birthdayTimeStamp = 0L
}