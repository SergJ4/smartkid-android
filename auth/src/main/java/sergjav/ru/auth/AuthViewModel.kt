package sergjav.ru.auth

import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.domain.interfaces.*
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger
) :
    BaseViewModel(router, messageBus, progressBus, logger) {

    fun login() = router.goTo(LOGIN_SCREEN)

    fun signUp() = router.goTo(FIRST_REGISTRATION_SCREEN)
}