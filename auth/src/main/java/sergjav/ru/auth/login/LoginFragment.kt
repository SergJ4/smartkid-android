package sergjav.ru.auth.login

import kotlinx.android.synthetic.main.login_screen.*
import sergjav.ru.auth.R
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.core.base.BaseFragment
import sergjav.ru.core.extensions.filter
import sergjav.ru.core.extensions.hideKeyboard
import sergjav.ru.core.extensions.invoke
import sergjav.ru.core.extensions.subscribe
import sergjav.ru.domain.AnyValue
import sergjav.ru.domain.model.Trigger
import toothpick.config.Module
import javax.inject.Inject

class LoginFragment : BaseFragment<LoginViewModel>() {
    override val contentView: Int
        get() = R.layout.login_screen

    override val viewModelClass: Class<LoginViewModel> = LoginViewModel::class.java

    override var modules: Array<Module> = arrayOf(LoginFragmentModule())

    @Inject
    internal lateinit var emailText: EmailText
    @Inject
    internal lateinit var passwordText: PasswordText
    @Inject
    internal lateinit var acceptClicked: AcceptClicked

    override fun setup() {
        email.addTextChangedListener(emailText)
        password.addTextChangedListener(passwordText)
        loginButton.setOnClickListener {
            it.hideKeyboard()
            viewModel.acceptButtonEnabled(false)
            acceptClicked(Trigger(AnyValue))
        }

        subscribeToViewModel()
    }

    private fun subscribeToViewModel() {
        viewModel
            .email
            .filter { it != email.text.toString() }
            .subscribe(lifecycle) {
                email.setText(it)
            }

        viewModel
            .password
            .filter { it != password.text.toString() }
            .subscribe(lifecycle) {
                password.setText(it)
            }

        viewModel
            .emailValidOutput
            .subscribe(lifecycle) {
                if (it == true) {
                    emailField.isErrorEnabled = false
                } else {
                    emailField.error = getString(R.string.email_not_valid)
                    emailField.isErrorEnabled = true
                }
            }

        viewModel
            .passwordValidOutput
            .subscribe(lifecycle) {
                if (it == true) {
                    passwordField.isErrorEnabled = false
                } else {
                    passwordField.error = getString(R.string.password_not_valid)
                    passwordField.isErrorEnabled = true
                }
            }

        viewModel
            .acceptButtonEnabled
            .subscribe(lifecycle) { isEnabled ->
                isEnabled?.let {
                    loginButton.isEnabled = it
                }
            }
    }

    companion object {
        fun getInstance() = LoginFragment()
    }
}