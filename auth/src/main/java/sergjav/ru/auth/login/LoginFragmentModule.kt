package sergjav.ru.auth.login

import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.auth.domain.usecase.EmailValid
import sergjav.ru.auth.domain.usecase.LoginUser
import sergjav.ru.auth.domain.usecase.PasswordValid
import toothpick.config.Module


class LoginFragmentModule : Module() {
    init {
        bind(EmailText::class.java).singletonInScope()
        bind(PasswordText::class.java).singletonInScope()
        bind(AcceptClicked::class.java).singletonInScope()
        bind(EmailValid::class.java).singletonInScope()
        bind(PasswordValid::class.java).singletonInScope()
        bind(LoginUser::class.java).singletonInScope()
    }
}