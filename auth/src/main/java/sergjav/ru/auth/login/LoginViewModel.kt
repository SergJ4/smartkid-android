package sergjav.ru.auth.login

import android.arch.lifecycle.MutableLiveData
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.auth.domain.usecase.EmailValid
import sergjav.ru.auth.domain.usecase.LoginUser
import sergjav.ru.auth.domain.usecase.PasswordValid
import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.core.extensions.invoke
import sergjav.ru.core.extensions.startWith
import sergjav.ru.domain.exceptions.defaultErrorHandler
import sergjav.ru.domain.interfaces.*
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger,
    emailText: EmailText,
    passwordText: PasswordText,
    emailValid: EmailValid,
    passwordValid: PasswordValid,
    loginUser: LoginUser
) : BaseViewModel(router, messageBus, progressBus, logger) {

    internal val email = MutableLiveData<String>()
    internal val password = MutableLiveData<String>()
    internal val emailValidOutput = MutableLiveData<Boolean>().startWith(true)
    internal val passwordValidOutput = MutableLiveData<Boolean>().startWith(true)
    internal val acceptButtonEnabled = MutableLiveData<Boolean>().startWith(true)

    init {
        compositeDisposable
            .add(emailText().subscribe { email(it) })

        compositeDisposable
            .add(passwordText().subscribe { password(it) })

        emailValid().subscribeAsync {
            emailValidOutput(it)
            acceptButtonEnabled(true)
        }

        passwordValid().subscribeAsync {
            passwordValidOutput(it)
            acceptButtonEnabled(true)
        }

        loginUser()
            .doOnError { throwable ->
                defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
                progressBus.showProgress(false)
                logger.e(throwable)
                acceptButtonEnabled(true)
            }
            .retry()
            .subscribeAsync {
                progressBus.showProgress(false)
                router.goTo(PROFILE_SCREEN)
            }
    }
}
