package sergjav.ru.auth

import kotlinx.android.synthetic.main.auth_screen.*
import sergjav.ru.core.base.BaseFragment

class AuthFragment : BaseFragment<AuthViewModel>() {

    override val contentView: Int = R.layout.auth_screen

    override val viewModelClass: Class<AuthViewModel> = AuthViewModel::class.java

    override fun setup() {
        loginButton.setOnClickListener { viewModel.login() }
        signupButton.setOnClickListener { viewModel.signUp() }
    }

    companion object {
        fun getInstance() = AuthFragment()
    }
}