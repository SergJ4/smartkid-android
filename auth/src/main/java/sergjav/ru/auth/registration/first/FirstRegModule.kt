package sergjav.ru.auth.registration.first

import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.NameText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.auth.domain.usecase.EmailValid
import sergjav.ru.auth.domain.usecase.NameValid
import sergjav.ru.auth.domain.usecase.PasswordValid
import toothpick.config.Module


class FirstRegModule : Module() {
    init {
        bind(EmailText::class.java).singletonInScope()
        bind(PasswordText::class.java).singletonInScope()
        bind(NameText::class.java).singletonInScope()
        bind(AcceptClicked::class.java).singletonInScope()
        bind(EmailValid::class.java).singletonInScope()
        bind(PasswordValid::class.java).singletonInScope()
        bind(NameValid::class.java).singletonInScope()
    }
}