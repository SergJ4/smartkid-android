package sergjav.ru.auth.registration.income

import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.WeekDayValue
import sergjav.ru.core.domain.IncomeValue
import toothpick.config.Module


class IncomeModule : Module() {

    init {
        bind(AcceptClicked::class.java).singletonInScope()
        bind(IncomeValue::class.java).singletonInScope()
        bind(WeekDayValue::class.java).singletonInScope()
    }
}