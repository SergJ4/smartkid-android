package sergjav.ru.auth.registration.income

import android.arch.lifecycle.MutableLiveData
import sergjav.ru.auth.domain.WeekDayValue
import sergjav.ru.auth.domain.usecase.RegisterNewUser
import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.core.domain.IncomeValue
import sergjav.ru.core.domain.usecase.ChangeUserIncome
import sergjav.ru.core.extensions.invoke
import sergjav.ru.core.extensions.startWith
import sergjav.ru.domain.exceptions.defaultErrorHandler
import sergjav.ru.domain.interfaces.*
import javax.inject.Inject

class IncomeViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger,
    incomeValue: IncomeValue,
    weekDayValue: WeekDayValue,
    registerNewUser: RegisterNewUser,
    private val changeUserIncome: ChangeUserIncome
) :
    BaseViewModel(router, messageBus, progressBus, logger) {

    internal val income = MutableLiveData<String>()
    internal val weekDay = MutableLiveData<Int>()
    internal val acceptButtonEnabled = MutableLiveData<Boolean>().startWith(true)

    init {
        compositeDisposable
            .add(incomeValue().subscribe { income(it) })

        compositeDisposable
            .add(weekDayValue().subscribe { weekDay(it) })

        registerNewUser()
            .flatMap { incomeValue().take(1).singleOrError() }
            .map {
                val incomeString = if (it.endsWith(".")) {
                    "${it}0"
                } else {
                    it
                }
                return@map incomeString.toDoubleOrNull() ?: 0.0
            }
            .doOnError { throwable ->
                defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
                progressBus.showProgress(false)
                logger.e(throwable)
                acceptButtonEnabled(true)
            }
            .retry()
            .subscribeAsync { income ->
                if (income > 0) {
                    changeIncome(income)
                } else {
                    goToProfile()
                }
            }
    }

    private fun changeIncome(income: Double) = changeUserIncome(income)
        .subscribeAsync { _ -> goToProfile() }

    private fun goToProfile() {
        progressBus.showProgress(false)
        router.goTo(PROFILE_SCREEN)
    }
}