package sergjav.ru.auth.registration.first

import android.arch.lifecycle.MutableLiveData
import sergjav.ru.auth.domain.EmailText
import sergjav.ru.auth.domain.NameText
import sergjav.ru.auth.domain.PasswordText
import sergjav.ru.auth.domain.RegistrationDataContainer
import sergjav.ru.auth.domain.usecase.EmailValid
import sergjav.ru.auth.domain.usecase.FirstRegValidationResult
import sergjav.ru.auth.domain.usecase.NameValid
import sergjav.ru.auth.domain.usecase.PasswordValid
import sergjav.ru.core.SchedulersProvider
import sergjav.ru.core.base.BaseViewModel
import sergjav.ru.core.extensions.invoke
import sergjav.ru.core.extensions.startWith
import sergjav.ru.domain.exceptions.defaultErrorHandler
import sergjav.ru.domain.interfaces.*
import javax.inject.Inject

class FirstRegViewModel @Inject constructor(
    router: Router,
    messageBus: MessageBus,
    progressBus: ProgressBus,
    logger: Logger,
    emailValid: EmailValid,
    passwordValid: PasswordValid,
    nameValid: NameValid,
    private val registrationDataContainer: RegistrationDataContainer,
    firstRegValidationResult: FirstRegValidationResult,
    emailText: EmailText,
    passwordText: PasswordText,
    nameText: NameText
) : BaseViewModel(router, messageBus, progressBus, logger) {

    internal val email = MutableLiveData<String>()
    internal val password = MutableLiveData<String>()
    internal val name = MutableLiveData<String>()
    internal val emailValidOutput = MutableLiveData<Boolean>().startWith(true)
    internal val passwordValidOutput = MutableLiveData<Boolean>().startWith(true)
    internal val nameValidOutput = MutableLiveData<Boolean>().startWith(true)
    internal val acceptButtonEnabled = MutableLiveData<Boolean>().startWith(true)

    init {
        compositeDisposable
            .add(emailText().subscribe { email(it) })

        compositeDisposable
            .add(passwordText().subscribe { password(it) })

        compositeDisposable
            .add(nameText().subscribe { name(it) })

        emailValid().subscribeAsync { emailValidOutput(it) }

        passwordValid()
            .subscribeAsync(scheduler = SchedulersProvider.computation()) {
                passwordValidOutput(it)
            }

        nameValid()
            .subscribeAsync(scheduler = SchedulersProvider.computation()) {
                nameValidOutput(it)
            }

        firstRegValidationResult()
            .doOnError { throwable ->
                defaultErrorHandler(throwable) { messageBus.showMessage(message = it) }
                progressBus.showProgress(false)
                logger.e(throwable)
                acceptButtonEnabled(true)
            }
            .retry()
            .subscribeAsync { valid ->
                progressBus.showProgress(false)
                if (valid) {
                    handleResult()
                } else {
                    acceptButtonEnabled(true)
                }
            }
    }

    private fun handleResult() {
        registrationDataContainer.email = email.value!!
        registrationDataContainer.password = password.value!!
        registrationDataContainer.name = name.value!!
        router.goTo(INCOME_REGISTRATION_SCREEN)
    }
}