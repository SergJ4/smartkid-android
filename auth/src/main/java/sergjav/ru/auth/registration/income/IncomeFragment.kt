package sergjav.ru.auth.registration.income

import kotlinx.android.synthetic.main.third_registration_screen.*
import sergjav.ru.auth.R
import sergjav.ru.auth.domain.AcceptClicked
import sergjav.ru.auth.domain.WeekDayValue
import sergjav.ru.core.base.BaseFragment
import sergjav.ru.core.domain.IncomeValue
import sergjav.ru.core.extensions.filter
import sergjav.ru.core.extensions.invoke
import sergjav.ru.core.extensions.subscribe
import sergjav.ru.domain.AnyValue
import sergjav.ru.domain.model.Trigger
import toothpick.config.Module
import javax.inject.Inject


class IncomeFragment : BaseFragment<IncomeViewModel>() {
    override val contentView: Int = R.layout.third_registration_screen
    override val viewModelClass: Class<IncomeViewModel> = IncomeViewModel::class.java
    override var modules: Array<Module> = arrayOf(IncomeModule())

    @Inject
    internal lateinit var acceptClicked: AcceptClicked
    @Inject
    internal lateinit var incomeValue: IncomeValue
    @Inject
    internal lateinit var weekDayValue: WeekDayValue

    override fun setup() {
        income.addTextChangedListener(incomeValue)
        weekDaySelector.minValue = 1
        weekDaySelector.maxValue = 7
        weekDaySelector.setOnValueChangedListener(weekDayValue)
        acceptButton.setOnClickListener {
            viewModel.acceptButtonEnabled(false)
            acceptClicked(Trigger(AnyValue))
        }

        subscribeToViewModel()
    }

    private fun subscribeToViewModel() {
        viewModel
            .income
            .filter { it != income.text.toString() }
            .subscribe(lifecycle) {
                income.setText(it ?: "")
            }

        viewModel
            .weekDay
            .filter { it != weekDaySelector.value }
            .subscribe(lifecycle) {
                weekDaySelector.value = it ?: 0
            }

        viewModel
            .acceptButtonEnabled
            .subscribe(lifecycle) { isEnabled ->
                isEnabled?.let {
                    acceptButton.isEnabled = it
                }
            }
    }

    companion object {
        fun getInstance() = IncomeFragment()
    }
}