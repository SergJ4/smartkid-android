package sergjav.ru.repository.interceptors

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject
import sergjav.ru.domain.exceptions.SimpleMessageException
import sergjav.ru.repository.R

/**
 * Здесь мы проверяем наличие ошибок в самом ответе сервера.
 * Если таковые имеются, то оборачиваем их в ApolloException и отдаем
 * в onFailure() метод callback`а
 * Этот exception далее попадает в onError метод подписчика (Subscriber)
 * По умолчанию обработка происходит в [BaseViewModel] в дефолтном параметре onError методов
 * subscribeAsync(), но любой подписчик может переопределить это поведение, хотя нежелательно.
 */
internal class ResponseStripInterceptor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        val response = chain.proceed(builder.build())

        val contentType = response.body()?.contentType()
        val bodyString = response.body()?.string()

        if (bodyString.isNullOrBlank()) {
            throw SimpleMessageException(exceptionMessage = context.getString(R.string.unknown_error))
        }

        val jsonObject = JSONObject(bodyString)
        val success = jsonObject.getBoolean("success")

        if (!success) {
            throw SimpleMessageException(exceptionMessage = jsonObject.getString("message"))
        }

        val strippedResponse = jsonObject.getJSONObject("payload").toString()

        val body = ResponseBody.create(contentType, strippedResponse)

        return response.newBuilder().body(body).build()
    }
}