package sergjav.ru.repository.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import java.util.*

internal class LocaleHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var countryCode = Locale.getDefault().country
        if (countryCode.isBlank()) {
            countryCode = "EN"
        }

        val originalRequest = chain.request()
        val builder =
            originalRequest.newBuilder().method(originalRequest.method(), originalRequest.body())
        builder.addHeader("X-LOCALE-HEADER", countryCode)
        return chain.proceed(builder.build())
    }
}