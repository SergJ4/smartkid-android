package sergjav.ru.repository.interceptors

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import sergjav.ru.domain.CONNECTION_ERROR
import sergjav.ru.domain.exceptions.SimpleMessageException
import sergjav.ru.repository.BuildConfig
import sergjav.ru.repository.R
import sergjav.ru.repository.extensions.hasNetwork
import timber.log.Timber
import java.io.IOException
import java.net.UnknownHostException
import javax.net.ssl.SSLException

/**
 * Перехватчик для определения наличия сетевого подключения на девайсе
 * и наличие ошибок диапазона 400-599 в ответе.
 * Все сгенерированные здесь исключения далее по цепочке попадают
 * в [ErrorsInterceptor]
 */
internal class ConnectivityInterceptor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        //Проверяем наличие сети
        if (!context.hasNetwork()) {
            throw SimpleMessageException(
                errorId = CONNECTION_ERROR,
                exceptionMessage = context.getString(R.string.no_connection_error)
            )
        }

        val builder = chain.request().newBuilder()
        try {
            val response = chain.proceed(builder.build())
            //Проверяем код ответа на наличие ошибок
            if ((response.code() >= 400) and (response.code() < 600)) {
                throw SimpleMessageException(
                    response.code(),
                    context.getString(R.string.unknown_error)
                )
            }
            return response
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                Timber.e(e)
            }
            when {
                (e is UnknownHostException || e is SSLException) -> throw SimpleMessageException(
                    errorId = CONNECTION_ERROR,
                    exceptionMessage = context.getString(R.string.no_connection_error)
                )
                e is SimpleMessageException -> throw e
                else -> throw SimpleMessageException(0, context.getString(R.string.unknown_error))
            }
        }
    }
}
