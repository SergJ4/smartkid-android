package sergjav.ru.repository.services

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import sergjav.ru.repository.models.ApiResult
import sergjav.ru.repository.models.ApiUser

internal interface AuthService {

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<ApiUser>

    @FormUrlEncoded
    @POST("new-user")
    fun registerUser(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("name") name: String,
        @Field("birthdayTimestamp") birthdayTimestamp: Long,
        @Field("dayNumToIncrementMoney") dayNumToIncrementMoney: Int
    ): Single<ApiUser>

    @FormUrlEncoded
    @POST("validateEmail")
    fun validateEmail(@Field("email") email: String): Single<ApiResult<Boolean>>
}