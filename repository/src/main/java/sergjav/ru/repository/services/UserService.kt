package sergjav.ru.repository.services

import io.reactivex.Single
import retrofit2.http.*
import sergjav.ru.repository.models.ApiUser

internal interface UserService {

    @GET("user/{id}")
    fun getUser(@Path("id") userId: String): Single<ApiUser>

    @POST("changeAmount/{id}")
    @FormUrlEncoded
    fun changeUsersMoneyAmount(
        @Path("id") userId: String,
        @Field("fromUserId") localUserId: String,
        @Field("amount") amount: Double
    ): Single<ApiUser>

    @POST("/changeIncome/{id}")
    @FormUrlEncoded
    fun changeUsersIncome(
        @Path("id") userId: String,
        @Field("amount") amount: Double
    ): Single<ApiUser>
}