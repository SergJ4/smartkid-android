package sergjav.ru.repository

import android.content.SharedPreferences
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import sergjav.ru.domain.interfaces.repository.LocalRepo
import javax.inject.Inject

class SharedPrefsRepo @Inject constructor(private val prefs: SharedPreferences) : LocalRepo {

    override fun save(key: String, value: Any) {
        val editor = prefs.edit()
        when (value) {
            is String -> editor.putString(key, value)
            is Int -> editor.putInt(key, value)
            is Float -> editor.putFloat(key, value)
            is Long -> editor.putLong(key, value)
            is Boolean -> editor.putBoolean(key, value)
        }

        editor.apply()
    }

    override fun get(key: String): Option<Any> =
        if (prefs.contains(key)) {
            Some(prefs.all[key]!!)
        } else {
            None
        }

    override fun exists(key: String): Boolean = prefs.contains(key)
}

