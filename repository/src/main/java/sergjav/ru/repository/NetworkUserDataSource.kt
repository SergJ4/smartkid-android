package sergjav.ru.repository

import io.reactivex.Single
import retrofit2.Retrofit
import sergjav.ru.domain.interfaces.repository.UserDataSource
import sergjav.ru.domain.model.DomainUser
import sergjav.ru.repository.models.toDomainModel
import sergjav.ru.repository.services.AuthService
import sergjav.ru.repository.services.UserService
import javax.inject.Inject

class NetworkUserDataSource @Inject constructor(private val retrofit: Retrofit) : UserDataSource {

    private val userService: UserService by lazy {
        retrofit.create(UserService::class.java)
    }

    private val authService: AuthService by lazy {
        retrofit.create(AuthService::class.java)
    }

    override fun getUserById(id: String): Single<DomainUser> =
        userService
            .getUser(id)
            .map { it.toDomainModel() }

    override fun registerNewUser(
        email: String,
        password: String,
        name: String,
        birthdayTimestamp: Long,
        dayNumToIncrementMoney: Int
    ): Single<DomainUser> =
        authService
            .registerUser(email, password, name, birthdayTimestamp, dayNumToIncrementMoney)
            .map { it.toDomainModel() }

    override fun login(email: String, password: String): Single<DomainUser> =
        authService
            .login(email, password)
            .map { it.toDomainModel() }

    override fun changeUserIncome(
        userId: String,
        newIncome: Double
    ): Single<DomainUser> =
        userService
            .changeUsersIncome(userId, newIncome)
            .map { it.toDomainModel() }

    override fun validateEmail(email: String): Single<Boolean> =
        authService
            .validateEmail(email)
            .map { it.result }
}