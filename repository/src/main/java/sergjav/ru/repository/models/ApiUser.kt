package sergjav.ru.repository.models

import sergjav.ru.domain.model.DomainUser

data class ApiUser(
    val id: String,
    val name: String,
    val birthdayTimestamp: Long,
    val wishes: List<ApiWish>,
    val moneyPerWeekIncome: Double,
    val currentMoneyAmount: Double,
    val moneyIncrementWeekDay: Int
)

fun ApiUser.toDomainModel() = DomainUser(
    id = id,
    name = name,
    birthdayTimestamp = birthdayTimestamp,
    moneyPerWeekIncome = moneyPerWeekIncome,
    currentMoneyAmount = currentMoneyAmount,
    moneyIncrementWeekDay = moneyIncrementWeekDay,
    wishes = wishes.map { it.toDomainModel() }
)