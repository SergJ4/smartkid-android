package sergjav.ru.repository.models


data class ApiResult<T>(val result: T)