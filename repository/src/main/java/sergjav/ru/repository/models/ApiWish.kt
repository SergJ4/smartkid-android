package sergjav.ru.repository.models

import sergjav.ru.domain.model.DomainWish


data class ApiWish(
    val id: String,
    val title: String,
    val price: Double
)

fun ApiWish.toDomainModel() = DomainWish(id, title, price)