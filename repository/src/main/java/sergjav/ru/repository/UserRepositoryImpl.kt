package sergjav.ru.repository

import arrow.core.Option
import io.reactivex.Observable
import io.reactivex.Single
import sergjav.ru.domain.interfaces.repository.LocalRepo
import sergjav.ru.domain.interfaces.repository.UserDataSource
import sergjav.ru.domain.interfaces.repository.UserRepository
import sergjav.ru.domain.model.DomainUser
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    override val userDataSource: UserDataSource,
    private val localRepo: LocalRepo
) : UserRepository {

    //todo подумать как сделать юзера по настоящему Observable
    override fun user(id: String): Observable<DomainUser> =
        userDataSource.getUserById(id).toObservable()

    override fun getLocalUser(): Single<DomainUser> =
        localRepo
            .getUserId()
            .fold(
                ifEmpty = {
                    throw IllegalStateException("Trying to fetch local user when not logged in!!!")
                },
                ifSome = {
                    userDataSource.getUserById(it)
                }
            )

    override fun registerNewUser(
        email: String,
        password: String,
        name: String,
        birthdayTimestamp: Long,
        dayNumToIncrementMoney: Int
    ): Single<DomainUser> = userDataSource
        .registerNewUser(email, password, name, birthdayTimestamp, dayNumToIncrementMoney)
        .flatMap {
            localRepo.saveUser(it.id)
            return@flatMap user(id = it.id).take(1).singleOrError()
        }

    override fun login(email: String, password: String): Single<DomainUser> =
        userDataSource
            .login(email, password)
            .doOnSuccess {
                localRepo.saveUser(it.id)
            }

    override fun isLoggedIn(): Boolean = localRepo.getUserId().isEmpty()

    override fun validateEmail(email: String): Single<Boolean> =
        userDataSource.validateEmail(email)

    override fun changeUserIncome(newIncome: Double): Single<DomainUser> =
        localRepo
            .getUserId()
            .fold(
                {
                    Single.error(IllegalStateException("Trying to change income when not logged in"))
                },
                {
                    userDataSource.changeUserIncome(it, newIncome)
                }
            )
}

private const val USER_ID = "user_id"

fun LocalRepo.saveUser(id: String) {
    save(USER_ID, id)
}

fun LocalRepo.getUserId() = get(USER_ID) as Option<String>
