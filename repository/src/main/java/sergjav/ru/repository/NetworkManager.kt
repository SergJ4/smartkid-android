package sergjav.ru.repository

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import sergjav.ru.repository.interceptors.ConnectivityInterceptor
import sergjav.ru.repository.interceptors.LocaleHeaderInterceptor
import sergjav.ru.repository.interceptors.ResponseStripInterceptor
import timber.log.Timber
import java.util.concurrent.TimeUnit

const val BASE_URL = "http://54.180.66.247"
const val HTTP_LOG_TAG = "SmartKidHttp"
const val NETWORK_TIMEOUT = 5

fun initRepository(context: Context): Retrofit {

    val connectivityInterceptor = ConnectivityInterceptor(context)
    val localeHeaderInterceptor = LocaleHeaderInterceptor()
    val errorsInterceptor = ResponseStripInterceptor(context)

    val okHttpBuilder = OkHttpClient.Builder()

    if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor { message ->
            Timber.tag(HTTP_LOG_TAG).d(message)
        }
            .setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpBuilder.addInterceptor(loggingInterceptor)
    }

    okHttpBuilder.addInterceptor(connectivityInterceptor)
    okHttpBuilder.addInterceptor(localeHeaderInterceptor)
    okHttpBuilder.addInterceptor(errorsInterceptor)

    okHttpBuilder.connectTimeout(NETWORK_TIMEOUT.toLong(), TimeUnit.SECONDS)
    okHttpBuilder.readTimeout(NETWORK_TIMEOUT.toLong(), TimeUnit.SECONDS)
    okHttpBuilder.writeTimeout(NETWORK_TIMEOUT.toLong(), TimeUnit.SECONDS)

    return Retrofit
        .Builder()
        .baseUrl(BASE_URL)
        .client(okHttpBuilder.build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}